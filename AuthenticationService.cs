﻿using System;
using Grpc.Core;
using Sila2.Utils;
using Sila2.Stratec.Core.Authenticationservice.V1;

namespace Roche.Dpcr.SilaClient
{
    public class AuthenticationServiceWrapper
    {
        private readonly AuthenticationService.AuthenticationServiceClient authenticationClient;

        public AuthenticationServiceWrapper(Channel channel)
        {
            this.authenticationClient = new AuthenticationService.AuthenticationServiceClient(channel);
        }

        public AccessToken Login(string username, string password)
        {
            try
            {
                var loginParameters = new Login_Parameters
                {
                    UserIdentification = new Sila2.Org.Silastandard.String { Value = username },
                    Password = new Sila2.Org.Silastandard.String { Value = password }
                };

                Login_Responses loginResponse = this.authenticationClient.Login(loginParameters);
                Console.WriteLine("Login succeeded.");
                Console.WriteLine($"Access-token is valid for {loginResponse.TokenLifetime.Value} seconds.");
                
                return new AccessToken(loginResponse.AccessToken.Value, loginResponse.TokenLifetime.Value);
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"Login failed (with error-meesage: '{silaExceptionMessage}'.");

                throw new Exception(silaExceptionMessage, ex);
            }
        }

        public void Logout(AccessToken accessToken)
        {
            try
            {
                var logoutParameters = new Logout_Parameters();
                Logout_Responses logoutResponses = this.authenticationClient.Logout(logoutParameters, accessToken.ToMetadata());
                Console.WriteLine("Logout succeeded.");
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"Logout failed (with error-meesage: '{silaExceptionMessage}'.");

                throw new Exception(silaExceptionMessage, ex);
            }
        }
    }
}
