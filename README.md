# sila-client-dpcr

### Overview

This repo contains the code for a simple SiLA-client that connects to a digital-lightcycler which acts as SiLA server.
The goal is simply to test the connection and features.
Nothing useful is done or displayed.

The project is written in Visual-Studio 2019; it compiles into a .NET core console application.

---

### Relevant Information

[only accessible in Roche network]
- Feature files and manual by Stratec: https://drive.google.com/drive/folders/1ny0-9vDcoo1wSpkW2bzS7qvVMk4u_AQX
- Server (virtual-machine) IP-Address: **10.21.241.60** (hostname: rscmvdpcr01.rsc.roche.com)
- Port **50052**
- User enabled for EDI: username: ````EDIuser````, password: ````EDIuser````
 
---

### Tracker how project was setup

- *Sila* project is not shared through ````nuget.org````, but at ````https://nexus.unitelabs.ch/repository/nuget-sila-development/````.
  Add this nuget repository, either by creating a ````NuGet.config````-file, or by adding it directly in Package-Manager settings. E.g.  
    ````
    key="nuget-sila" 
    value="https://nexus.unitelabs.ch/repository/nuget-sila-development/" 
    protocolVersion="2"
    ````


- Add the following nuget-packages:
  - **Google.Protobuf**  (shared through ````nuget.org````)
  - **Sila2** (from dedicated nuget repo, see above)(brings dependency on **Grpc.Core**)
  - **Sila2.Tools** (from dedicated nuget repo, see above)


- Create ````features````-folder.  
  Add all feature files shared by the project into this folder.


- Edit project-file; add the following entry:
  ````
  <ItemGroup>
    <SilaFeature Include="features\DigitalLightCyclerStateProvider.sila.xml"></SilaFeature>
    <SilaFeature Include="features\AuthorizationService.sila.xml"></SilaFeature>
    <SilaFeature Include="features\AuthenticationService.sila.xml"></SilaFeature>
    <SilaFeature Include="features\DigitalLightCyclerDrawerController.sila.xml"></SilaFeature>
    <SilaFeature Include="features\DigitalLightCyclerRunController.sila.xml"></SilaFeature>
  </ItemGroup>
  ````

- Create folder ````cert```` and include ````rootCA.cer```` 
(which is shared by project together with feature files) into this folder.

- We need the certificate in pem-format. 
  Convert certificate using `*openssl* and add pem-formated cert also into `````cert`````-folder. 
  Set 'Build Action' of pem-file to 'Embedded resource'. 
  ````
  openssl x509 -in rootCA.cer -out rootCA.pem  
  ````
  



---

### Helpful Hints for SiLA-Beginners (as me)
- https://gitlab.com/SiLA2/sila_csharp
- https://gitlab.com/SiLA2/sila_csharp/-/wikis/Plate-Reader-CLI

