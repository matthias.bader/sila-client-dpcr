﻿using System;
using Grpc.Core;
using Sila2.Utils;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sila2.Stratec.State.Digitallightcyclerstateprovider.V1;

namespace Roche.Dpcr.SilaClient
{
    public class StateProviderServiceWrapper
    {
        private readonly DigitalLightCyclerStateProvider.DigitalLightCyclerStateProviderClient stateProviderClient;

        public StateProviderServiceWrapper(Channel channel)
        {
            this.stateProviderClient = new DigitalLightCyclerStateProvider.DigitalLightCyclerStateProviderClient(channel);
        }

        public async Task<string> GetInstrumentStateAsync()
        {
            var parameters = new Subscribe_InstrumentState_Parameters();
            using (var streamingCall = this.stateProviderClient.Subscribe_InstrumentState(parameters))
            {
                // just get state once - don't wait for updates...
                using (IAsyncStreamReader<Subscribe_InstrumentState_Responses> responseStream = streamingCall.ResponseStream)
                {
                    bool success = await responseStream.MoveNext(CancellationToken.None);
                    if (success)
                    {
                        return responseStream.Current.InstrumentState.Value;
                    }

                    return null;
                }
            }
        }

        public WorkflowState GetWorklowState(AccessToken accessToken)
        {
            try
            {
                var requestParameters = new GetWorkflowState_Parameters();
                GetWorkflowState_Responses responses = this.stateProviderClient.GetWorkflowState(requestParameters, accessToken.ToMetadata());

                return new WorkflowState(responses);
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"GetWorklowState failed (with error-meesage: '{silaExceptionMessage}'.");

                throw new Exception(silaExceptionMessage, ex);
            }
        }
    }

    public class WorkflowState
    {
        public WorkflowState(GetWorkflowState_Responses response)
        {
            this.CanStartIVDRun = response.CanStartIVDRun.Value;
            this.CanStartResearchRun = response.CanStartResearchRun.Value;
            this.IsPlateLoadingValid = response.IsPlateLoadingValid.Value;
            this.State = response.State.Value;
            this.EstimatedCompletingTime = response.EstimatedCompletingTime;
        }

        public bool CanStartIVDRun { get; }

        public bool CanStartResearchRun { get; }

        public bool IsPlateLoadingValid { get; }
        
        public string State { get; }

        public Sila2.Org.Silastandard.Timestamp EstimatedCompletingTime { get; }

        public string ToFormattedString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Workflow-state");
            sb.AppendLine($"- Can start IVD Run: {this.CanStartIVDRun}");
            sb.AppendLine($"- Can start Research Run: {this.CanStartResearchRun}");
            sb.AppendLine($"- Is plate loading valid: {this.IsPlateLoadingValid}");
            sb.AppendLine($"- State: {this.State}");

            return sb.ToString();
        }
    }
}
