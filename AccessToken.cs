﻿using Grpc.Core;
using Google.Protobuf;
using Sila2.Stratec.Core.Authorizationservice.V1;

namespace Roche.Dpcr.SilaClient
{
    public class AccessToken
    {
        private readonly string token;

        public AccessToken(string token, long tokenLifetime)
        {
            this.token = token;
            this.InitialTokenLifetime = tokenLifetime;
        }

        public Metadata ToMetadata()
        {
            // Material:
            // - Sila Specification - Part B: SiLA CLient Metadata
            // - AuthorizationService.sila - feature description
            // - class Sila2.Utils.SilaClientMetadata (in Sila assembly)

            // create entry
            var accessTokenMetadata = new Metadata_AccessToken();
            accessTokenMetadata.AccessToken = new Sila2.Org.Silastandard.String { Value = this.token };
            var entry = new Metadata.Entry("accesstoken-bin", accessTokenMetadata.ToByteArray());

            // wrap entry in metadata
            return new Metadata { entry };
        }

        public long InitialTokenLifetime { get; }
    }
}
