﻿using System;
using System.Net;
using Sila2.Discovery;
using Grpc.Core;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Sila2.Utils;

namespace Roche.Dpcr.SilaClient
{
    class Program
    {
        const string IpAddress = "10.21.241.60";
        const int PortNumber = 50052;
        const string Username = "EDIuser";
        const string Password = "EDIuser";

        static void Main(string[] args)
        {
            Console.WriteLine("Started...");

            // create channel
            string rootCertificate = GetRootCaCertificate();            
            var sslCredentials = new SslCredentials(rootCertificate);

            Channel channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(IpAddress), PortNumber, sslCredentials);
            Console.WriteLine($"Created channel to device with IP-Address {IpAddress}.");

            //DisplayServerInfo(channel);

            // use authentication feature to retrieve access-token.
            var authenticationService = new AuthenticationServiceWrapper(channel);
            AccessToken accessToken = authenticationService.Login(Username, Password);

            // open and close drawer...
            var drawerController = new DrawerController(channel);
            drawerController.OpenDrawer(accessToken);
            drawerController.CloseDrawer(accessToken);

            // get instrument state.
            var stateProviderSerivce = new StateProviderServiceWrapper(channel);
            try
            {
                var task = Task.Run(async () => await stateProviderSerivce.GetInstrumentStateAsync());
                var state = task.Result;
                Console.WriteLine($"Instrument state is: {state}.");
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"Instrument-State streaming failed (with error-message: '{silaExceptionMessage}'.");
            }

            // get workflow state
            var workflowState = stateProviderSerivce.GetWorklowState(accessToken);
            Console.WriteLine(workflowState.ToFormattedString());

            // logout
            authenticationService.Logout(accessToken);

            Console.WriteLine();
            Console.WriteLine("Program finished. Press any key to close.");
            Console.ReadKey();
        }

        private static string GetRootCaCertificate()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("rootCA.pem"));

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        private static void DisplayServerInfo(Channel channel)
        {
            Sila2.ServerData serverData = SiLADiscovery.GetServerData(channel);

            Console.WriteLine();
            Console.WriteLine($"Server-Config Name: {serverData.Config.Name}");
            Console.WriteLine($"Server-Config Uuid: {serverData.Config.Uuid}");
            Console.WriteLine($"Server-Info Type: {serverData.Info.Type}");
            Console.WriteLine($"Server-Info VendorURI: {serverData.Info.VendorURI}");
            Console.WriteLine($"Server-Info Version: {serverData.Info.Version}");
            Console.WriteLine($"Server-Info Description: {serverData.Info.Description}");

            foreach (Sila2.Feature feature in serverData.Features)
            {
                Console.WriteLine("-------------------");
                Console.WriteLine("DisplayName: " + feature.DisplayName);
                Console.WriteLine("Description: " + feature.Description);
                Console.WriteLine("Category: " + feature.Category);
                Console.WriteLine("FeatureVersion: " + feature.FeatureVersion);
                Console.WriteLine("FullyQualifiedIdentifier: " + feature.FullyQualifiedIdentifier);
            }

            Console.WriteLine();
        }
    }
}
