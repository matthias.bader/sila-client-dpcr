﻿using System;
using Grpc.Core;
using Sila2.Utils;
using Sila2.Stratec.Control.Digitallightcyclerdrawercontroller.V1;

namespace Roche.Dpcr.SilaClient
{
    public class DrawerController
    {
        private readonly DigitalLightCyclerDrawerController.DigitalLightCyclerDrawerControllerClient client;

        public DrawerController(Channel channel)
        {
            this.client = new DigitalLightCyclerDrawerController.DigitalLightCyclerDrawerControllerClient(channel);
        }

        public void OpenDrawer(AccessToken accessToken)
        {
            try
            {
                var openDrawer_Parameters = new OpenDrawer_Parameters();
                OpenDrawer_Responses responses = client.OpenDrawer(openDrawer_Parameters, accessToken.ToMetadata());

                bool isSuccess = responses.OpenDrawerSuccess.Value;
                Console.WriteLine($"Open drawer success: {isSuccess}");
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"Open drawer failed (with error-meesage: '{silaExceptionMessage}'.");

                throw new Exception(silaExceptionMessage, ex);
            }
        }

        public void CloseDrawer(AccessToken accessToken)
        {
            try
            {
                var closeDrawer_Parameters = new CloseDrawer_Parameters();
                CloseDrawer_Responses responses = client.CloseDrawer(closeDrawer_Parameters, accessToken.ToMetadata());

                bool isSuccess = responses.CloseDrawerSuccess.Value;
                Console.WriteLine($"Close drawer success: {isSuccess}");
            }
            catch (Exception ex)
            {
                string silaExceptionMessage = ErrorHandling.HandleException(ex);
                Console.WriteLine($"Close drawer failed (with error-meesage: '{silaExceptionMessage}'.");

                throw new Exception(silaExceptionMessage, ex);
            }
        }
    }
}
